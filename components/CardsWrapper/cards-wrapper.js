import Card from '../Card';
import s from './cards-wrapper.module.css';

const CardsWrapper = (props) => {
  const { title, videos = [], size } = props;
  return (
    <section className={s.container}>
      <h2 className={s.title}>{title}</h2>
      <div className={s.cardWrapper}>
        {videos.map((v, idx) => {
          return <Card key={idx} id={idx} imgUrl={v.imgUrl} size={size} />;
        })}
      </div>
    </section>
  );
};

export default CardsWrapper;
