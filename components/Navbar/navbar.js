import Image from 'next/image';
import { useRouter } from 'next/router';
import Link from 'next/link';

import s from './navbar.module.css';
import { useState } from 'react';

const Navbar = (props) => {
  const { username } = props;
  const router = useRouter();
  const [showDropDown, setShowDropDown] = useState(false);

  const handleOnClickHome = (e) => {
    e.preventDefault();
    router.push('/');
  };

  const handleOnClickMylist = (e) => {
    e.preventDefault();
    router.push('/browser/my-list');
  };

  const handleShowDropdown = (e) => {
    e.preventDefault();
    setShowDropDown(!showDropDown);
  };

  return (
    <div className={s.container}>
      <div className={s.wrapper}>
        <Link href="/">
          <a className={s.logoLink}>
            <div className={s.logoWrapper}>
              <Image src="/static/netflix.svg" alt="Netflix logo" width="130px" height="34px" />
            </div>
          </a>
        </Link>

        <ul className={s.navItems}>
          <li onClick={handleOnClickHome} className={s.navItem}>
            Home
          </li>
          <li onClick={handleOnClickMylist} className={s.navItem2}>
            My List
          </li>
        </ul>
        <nav className={s.navContainer}>
          <div>
            <button className={s.usernameBtn} onClick={handleShowDropdown}>
              <p className={s.username}>{username}</p>
              <Image
                src="/static/expand_more.svg"
                alt="Expand more logo"
                width="24px"
                height="24px"
              />
            </button>
            {showDropDown && (
              <div className={s.navDropdown}>
                <div>
                  <Link href="/login">
                    <a className={s.linkName}>Sign Out</a>
                  </Link>

                  <div className={s.lineWrapper}></div>
                </div>
              </div>
            )}
          </div>
        </nav>
      </div>
    </div>
  );
};

export default Navbar;
